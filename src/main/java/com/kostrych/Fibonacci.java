package com.kostrych;

import java.util.Scanner;

/**
 * Resolves some task with Fibonacci numbers.
 *
 * @author Artem Kostrych
 * @version 1.0
 */

public class Fibonacci {
    /**
     * Constant.
     */
    public static final int ONE_HUNDRED = 100;
    /**
     * Field - start of interval.
     */
    private int startOfInterval;
    /**
     * Field - end of interval.
     */
    private int endOfInterval;

    /**
     * Constructor with parameters.
     *
     * @param start - start of he interval.
     * @param end   - end of he interval.
     */
    public Fibonacci(final int start, final int end) {
        startOfInterval = start;
        endOfInterval = end;
    }

    /**
     * Method main which is entry point of project.
     *
     * @param args console input parameters.
     */
    public static void main(final String[] args) {
        System.out.println("Please, enter the interval (for example:[1;100]):");
        Scanner scan = new Scanner(System.in);
        int start = scan.nextInt(), end = scan.nextInt();
        Fibonacci fibonacci = new Fibonacci(start, end);
        fibonacci.printOddAndEvenNumbers();
        fibonacci.printPercentage();
    }

    /**
     * Prints odd and even numbers.
     */
    public final void printOddAndEvenNumbers() {
        System.out.print("Odd numbers: ");
        int sumOfOddElements = 0;
        for (int i = startOfInterval; i <= endOfInterval; i++) {
            if (i % 2 == 1) {
                sumOfOddElements += i;
                if (i <= endOfInterval - 2) {
                    System.out.print(i + ", ");
                } else {
                    System.out.println(i + ";");
                }
            }
        }
        int sumOfEvenElements = 0;
        System.out.print("Sum of odd elements: " + sumOfOddElements
                + "\n\nEven elements: ");
        for (int i = endOfInterval; i >= startOfInterval; i--) {
            if (i % 2 == 0) {
                sumOfEvenElements += i;
                if (i > startOfInterval + 2) {
                    System.out.print(i + ", ");
                } else {
                    System.out.println(i + ";");
                }
            }
        }
        System.out.println("Sum of even elements: " + sumOfEvenElements);
    }

    /**
     * Print percentage of odd adn even Fibonacci numbers.
     */
    public final void printPercentage() {
        int firstNumber, secondNumber;
        if (endOfInterval % 2 == 1) {
            firstNumber = endOfInterval;
            secondNumber = endOfInterval - 1;
        } else {
            secondNumber = endOfInterval;
            firstNumber = endOfInterval - 1;
        }
        System.out.println("\nEnter size of set: ");
        Scanner scan = new Scanner(System.in);
        int sizeOfSet = scan.nextInt();
        int countOfOddElements = 1;
        int countOfEvenElements = 1;
        int currentElement = firstNumber + secondNumber;
        int penultElement = secondNumber;
        for (int i = 0; i < sizeOfSet - 2; i++) {
            if (currentElement % 2 == 0) {
                countOfEvenElements++;
            } else {
                countOfOddElements++;
            }
            int copy = penultElement;
            penultElement = currentElement;
            currentElement = copy + penultElement;
        }
        System.out.println("Percentage of odd elements = "
                + (double) countOfOddElements / sizeOfSet * ONE_HUNDRED
                + "%;\n" + "Percentage of even elements = "
                + (double) countOfEvenElements / sizeOfSet * ONE_HUNDRED
                + "%;");
    }
}
